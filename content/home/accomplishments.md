  +++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "Microsoft"
  organization_url = "https://www.linkedin.com/company/microsoft/"
  title = "Analyzing and Visualizing Data with Microsoft Excel"
  url = ""
  certificate_url = ""
  date_start = "2020-05-01"
  date_end = ""
  description = ""

[[item]]
  organization = "Project Management Institute"
  organization_url = "https://www.pmi.org/"
  title = "Project Management Professional (PMP)"
  url = ""
  certificate_url = ""
  date_start = "2020-02-01"
  date_end = ""
  description = ""
  
[[item]]
  organization = "DataCamp"
  organization_url = "https://www.datacamp.com"
  title = "Object-Oriented Programming in R: S3 and R6 Course"
  url = ""
  certificate_url = "https://www.datacamp.com"
  date_start = "2017-07-01"
  date_end = "2017-12-21"
  description = ""

+++
