+++
# Slider widget.
widget = "slider"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval = false

# Slide height (optional).
# E.g. `500px` for 500 pixels or `calc(100vh - 70px)` for full screen.
height = "500px"

# Slides.
# Duplicate an `[[item]]` block to add more slides.
[[item]]
  title = "Instagram Bot"
  content = ""
  align = "left"  # Choose `center`, `left`, or `right`.
  
#  overlay_color = "#666"  # An HTML color value.
  overlay_img = "instagram_bot.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0  # Darken the image. Value in range 0-1.

  cta_label = "Instagram Bot Repo"
  cta_url = "https://github.com/RichardBoulet/Insta_Roger_Bot"
  cta_icon_pack = "fab"
  cta_icon = "github"
  
[[item]]
  title = "Amtrak Web Scraper"
  content = ""
  align = "left"

#  overlay_color = "#555"  # An HTML color value.
  overlay_img = "web_scraper.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0  # Darken the image. Value in range 0-1.

  cta_label = "Web Scraping Repo"
  cta_url = "https://github.com/RichardBoulet/Amtrak_Ridership_Crawl"
  cta_icon_pack = "fab"
  cta_icon = "github"
  
[[item]]
  title = "Real-time Stock Price Prediction Application"
  content = ""
  align = "left"

#  overlay_color = "#333"  # An HTML color value.
  overlay_img = "commodity.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0  # Darken the image. Value in range 0-1.
  
  cta_label = "Stock Price Prediction App"
  cta_url = "https://share.streamlit.io/richardboulet/stock_predictor_streamlit/first_app.py"
  cta_icon_pack = "fas"
  cta_icon = "chart-line"
  
+++
